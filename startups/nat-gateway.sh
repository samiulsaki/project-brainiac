#! /bin/bash

# Script: Nat-Gateway Startup

sudo su -
sudo apt-get update
sudo timedatectl set-timezone Europe/Oslo # Sets the timezone
sleep 5
# Enable ip forwarding for nat-gateway
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
sudo iptables -t nat -A POSTROUTING -o ens4 -j MASQUERADE
