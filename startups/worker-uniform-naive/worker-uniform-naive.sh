#! /bin/bash

# Script: Worker Startup

sudo su -
sudo apt-get update
sudo timedatectl set-timezone Europe/Oslo # Sets the timezone
sleep 5
apt-get install -y git unzip bc jq nginx # Install packages
ufw allow 'Nginx HTTP'
systemctl start nginx # Starts Nginx

# Setup Consul
cd /tmp/
wget https://releases.hashicorp.com/consul/1.0.6/consul_1.0.6_linux_amd64.zip
unzip consul_1.0.6_linux_amd64.zip
mv consul /usr/local/bin
mkdir -p /var/lib/consul/
mkdir -p /usr/share/consul
mkdir -p /etc/consul.d
cd ~

echo '{"service": {"name": "webserver", "tags": ["HTTP"], "port": 80}}' | sudo tee /etc/consul.d/webserver.json # Creates a web server

worker_hostname=$(hostname)
worker_ip=$(hostname -I)
worker_dc=$(hostname | cut -c8-10)

nohup sudo consul agent -server=false -data-dir=/tmp/consul -node=${worker_hostname} -bind=${worker_ip} -enable-script-checks=true -datacenter ${worker_dc} -config-dir=/etc/consul.d &>/dev/null & sleep 3 && consul join consul-master-${worker_dc} # Registers to the consul-cluster as client
echo 'worker_hostname=$(hostname) && worker_ip=$(hostname -I) && worker_dc=$(hostname | cut -c8-10) && nohup sudo consul agent -server=false -data-dir=/tmp/consul -node=${worker_hostname} -bind=${worker_ip} -enable-script-checks=true -datacenter ${worker_dc} -config-dir=/etc/consul.d &>/dev/null & sleep 3 && consul join consul-master-${worker_dc} && exit 0' | sudo tee /etc/rc.local # Save the above command at system startup

cd /tmp/
git clone https://github.com/samiulsaki/project-brainiac.git
nohup sudo /bin/bash -c 'while [ true ]; do sleep 30; /tmp/project-brainiac/sidekicks/csv-gen.sh; done' &>/dev/null & disown # Starts CSV generator script in worker instance
nohup sudo /bin/bash -c 'while [ true ]; do timer=$(( ( RANDOM % 300 )  + 300 )); sleep $timer; /tmp/project-brainiac/startups/worker-uniform-naive/migrate-uniform-naive.sh; done' &>/dev/null & disown # Starts the Biased migration script in worker instance