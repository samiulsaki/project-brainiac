#! /bin/bash

# Copyright: Freeware (any user can download or distribute this script)
# Run the code at your own risk. Follow the instructions in README.
# Author: Samiul Saki
# Email: samiulsaki@gmail.com

# Script: Migration Biased [move to the worst datacentre with highest probability]

# Gets initial values
serv_account="my-instances@samiulsaki-148219.iam.gserviceaccount.com"
own_hostname=$(hostname)
own_dc=$(hostname | cut -c8-10)
own_zone=$(gcloud compute instances list | grep $own_hostname | awk '{print $2}')

CSVS=$(ls /home/ubuntu/) # Reads through all the lb logs for alive data-centre (dc) created by csv-gen script
avg_resp=0
declare -A array_temp # Temporary array for response time

for argh in $CSVS; do
    resp=0
    for i in $(tail -n 50 /home/ubuntu/${argh}); do # Read the last 50 entries (average response time)
        resp=$(( resp + i ))
    done
    eval "var=$(echo $argh | sed "s/lb-//g" | sed "s/.csv//g")" # Create a variable with dc name
    eval "${var}=$resp" # Add the response time to corresponding dc varible
    avg_resp=$((avg_resp + resp ))
    array_temp+=(["$var"]="${resp}") # Add the response time to temporary array with dc variable as index
done

avg_resp=$(( avg_resp / $(echo "${CSVS}" | wc -l) )) # Calculates average total response time of all the alive dc
own_resp=$((own_dc))
declare -A array_prob # Array for probability

for i in "${!array_temp[@]}"; do
    x=${array_temp[$i]}
	if [ $x -eq "0" ]
	then
		x=$((1))
	fi
    if [ $x != 0 ] && [ "$i" != "$own_dc" ] # Includes all the dc response time except own dc
    then
        c=$(echo "($avg_resp - $x) / $avg_resp" | bc -l) # Calculates the probability to move for each dc
        array_prob+=(["$i"]="${c}") # Add the probability to probability array for with dc variable as index
    fi
done

# Choose a dc probability as initial maximum probability
t_keys=(${!array_prob[@]})
t_size=${#array_prob[@]}
t_random_index=$(( RANDOM % t_size ))
cand_dc="${t_keys[${t_random_index}]}"
max=${array_prob[$cand_dc]}

for j in "${!array_prob[@]}"; do
	u=$(echo "${array_prob[$j]}" | bc -l)
	v=$(echo "$max" | bc -l)
	w=$(echo "$u < $v" | bc -l)
	if [ $w == '1' ]; then # Finds the probability that it closest to moving.
		max=$j
		cand_dc=$j # Choose the candidate zone with highest probability to move
	fi
done

cand_zone=$(gcloud compute instances list | grep lb-$cand_dc | awk '{print $2}') # Finds the gcloud region of the candidate zone
cand_dc_max_inst=$(gcloud compute instances list | grep worker-$cand_dc | awk '{print $1}' | grep -o '[0-9]*$' | wc -l) # Finds the existing number of worker instances in dc
own_dc_alive_inst=$(gcloud compute instances list | grep worker-$own_dc |  grep -v TERMINATED | grep -v STOPPING | awk '{print $1}' | grep -o '[0-9]*$' | wc -l) # Finds the exsiting number of alive worker instances in own dc
random=$(( ( RANDOM % 1000 )  + 1 ))

migrate_thres=3 # Set in minimum threshold limit for how many VMs per zone
migrate_resp=1
temp_avg=$( echo "$avg_resp / 1000" | bc -l)
temp_limit=$( echo "$migrate_resp /1000" | bc -l ) # Set in threshold for total system average response time to migrate

# If own dc have more than 3 alive worker instances, if candidate dc is not same as own dc, if own response time is less than average total response time, if system total average response time is higher than own zone average response time
if [ $own_dc_alive_inst -gt $migrate_thres ] && [ $cand_dc != $own_dc ] && [ $own_resp -lt $avg_resp ] && (( $(echo "$temp_avg > $temp_limit" | bc -l) ))
then
    eval "gcloud -q compute instances create worker-$cand_dc-$((cand_dc_max_inst + 1))-$random --network default --no-address --image ubuntu-console --machine-type f1-micro --zone $cand_zone --tags no-ip-$cand_dc --service-account $serv_account --scopes cloud-platform --metadata-from-file startup-script=/tmp/project-brainiac/startups/worker-biased/worker-biased.sh" # Creates a new worker instance at candidate dc
    eval "consul leave -http-addr=127.0.0.1:8500" # Creates a new worker instance at candidate dc
    sleep 2
    eval "gcloud -q compute instances delete $own_hostname --zone $own_zone" # Delete itself from gcloud
fi