#! /bin/bash

# Script: Load-Balancer Startup

sudo su -
sudo apt-get update
add-apt-repository ppa:vbernat/haproxy-1.7 -y
sudo timedatectl set-timezone Europe/Oslo # Sets the timezone
sleep 5
apt-get update
apt-get install -y haproxy git unzip jq # Install packages
cd /tmp/

# Setup Consul
wget https://releases.hashicorp.com/consul/1.0.6/consul_1.0.6_linux_amd64.zip
unzip consul_1.0.6_linux_amd64.zip
mv consul /usr/local/bin
mkdir -p /var/lib/consul/
mkdir -p /usr/share/consul
mkdir -p /etc/consul.d
cd ~

lb_hostname=$(hostname)
lb_ip=$(hostname -I)
lb_dc=$(hostname | sed "s/lb-//g")
consul_ip=$(gcloud compute instances list | tail -n+2 | grep consul-master-${lb_dc} | awk '{print $4}')

# Configure instance as consul server in the cluster and add as the service
cat <<EOF > /etc/systemd/system/consul.service
[Unit]
Description=Consul
Documentation=https://www.consul.io/

[Service]
ExecStart=/usr/local/bin/consul agent -server -join=${consul_ip} -data-dir=/tmp/consul -node=${lb_hostname} -bind=${lb_ip} -datacenter ${lb_dc} -config-dir=/etc/consul.d/
ExecReload=/bin/kill -HUP $MAINPID
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
EOF

# Consul service started and added to system startup
sudo systemctl daemon-reload && sudo systemctl start consul.service && sudo systemctl enable consul.service

cd /tmp/
# Setup Consul-HAProxy
wget https://github.com/hashicorp/consul-haproxy/releases/download/v0.2.0/consul-haproxy_0.2.0_linux_amd64.tar.gz
tar -xvzf consul-haproxy_0.2.0_linux_amd64.tar.gz
sudo chmod +x consul-haproxy_0.2.0_linux_amd64/consul-haproxy
mv consul-haproxy_0.2.0_linux_amd64/consul-haproxy /usr/local/bin/
cd ~

# Initiate a HAProxy template
cat <<EOF> /etc/consul.d/consul_ha.cfg
global
        log /dev/log    local0
        log /dev/log    local1 notice
        chroot /var/lib/haproxy
        stats socket /run/haproxy/admin.sock mode 660 level admin
        stats timeout 30s
        user haproxy
        group haproxy
        daemon
        # Default SSL material locations
        ca-base /etc/ssl/certs
        crt-base /etc/ssl/private
        # Default ciphers to use on SSL-enabled listening sockets.
        # For more information, see ciphers(1SSL). This list is from:
        #  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
        # An alternative list with additional directives can be obtained from
        #  https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=haproxy
        ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:$
        ssl-default-bind-options no-sslv3
defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back

backend http_back
   balance roundrobin
   {{range .c}}
   {{.}}{{end}}

listen stats
   bind *:9000
   mode http
   stats enable
   stats show-node
   stats show-legends
   stats refresh 20s
   stats uri /
   stats realm HAproxy-Statistics
   stats hide-version
EOF

systemctl restart haproxy
# Start a background process for consul-haproxy to add the backend-servers dynamically when the a worker instance register/deregister
nohup sudo consul-haproxy -addr=localhost:8500 -in /etc/consul.d/consul_ha.cfg -backend "c=webserver@${lb_dc}:80" -out /etc/haproxy/haproxy.cfg -reload "/etc/init.d/haproxy restart" &>/dev/null & disown 
echo 'lb_dc=$(hostname | sed "s/lb-//g") && nohup sudo consul-haproxy -addr=localhost:8500 -in /etc/consul.d/consul_ha.cfg -backend "c=webserver@${lb_dc}:80" -out /etc/haproxy/haproxy.cfg -reload "/etc/init.d/haproxy restart" &>/dev/null & disown && exit 0' | sudo tee /etc/rc.local # Set the above command in the system startup
