#! /bin/bash

# Script: Consul Startup

sudo su -
sudo apt-get update
sudo timedatectl set-timezone Europe/Oslo # Sets the timezone
sleep 5
apt-get install -y git unzip curl jq # Install packages
cd /tmp/

# Setup Consul
wget https://releases.hashicorp.com/consul/1.0.6/consul_1.0.6_linux_amd64.zip
unzip consul_1.0.6_linux_amd64.zip
mv consul /usr/local/bin
mkdir -p /var/lib/consul/
mkdir -p /usr/share/consul
mkdir -p /etc/consul.d
cd ~
consul_hostname=$(hostname)
consul_ip=$(hostname -I)
consul_dc=$(hostname | sed "s/consul-master-//g")

cat <<EOF > /etc/consul.d/ui.json
{
  "addresses": {
    "http": "0.0.0.0"
  }
}
EOF

# Configure instance as consul server in the cluster and add as the service
cat <<EOF > /etc/systemd/system/consul.service
[Unit]
Description=Consul
Documentation=https://www.consul.io/

[Service]
ExecStart=/usr/local/bin/consul agent -server -ui -bootstrap-expect=2 -data-dir=/tmp/consul -node=${consul_hostname} -bind=${consul_ip} -datacenter ${consul_dc} -config-dir=/etc/consul.d/
ExecReload=/bin/kill -HUP $MAINPID
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
EOF

# Consul service started and added to system startup
sudo systemctl daemon-reload && sudo systemctl start consul.service && sudo systemctl enable consul.service
consul members
