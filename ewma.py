#!/usr/bin/python

import os, sys, re
import numpy as np
import csv
import requests
import json
import time
import urllib.request
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt

file=sys.argv[1]

fmt = "%Y%m%d-%H%M%S"
df = pd.read_table(file, sep=',', skiprows=1, skipfooter=0, engine='python',
                   names=['Ptime', 'DC1_Resp', 'DC1_Worker', 'DC2_Resp', 'DC2_Worker', 'DC3_Resp', 'DC3_Worker'],
                   parse_dates=True,
                   date_parser=lambda s: datetime.strptime(s, fmt),
                   index_col=0)


def plot(DC1=False, DC1_raw=False, DC1_ma=False, DC1_ewma=False, DC2=False, DC2_raw=False, DC2_ma=False, DC2_ewma=False, DC3=False, DC3_raw=False, DC3_ma=False, DC3_ewma=False, window_span=None, periods=None, show=False, savefig=False, scheme='', label=''):
    plt.figure()
    plt.title(label)
    if DC1:
        dc1_resp = df['DC1_Resp']
        if DC1_raw:
            dc1_resp.plot(label='Raw data DC1') 
        if DC1_ma:       
            roll1 = dc1_resp.rolling( center=False, window=window_span, min_periods=periods).mean()
            roll1.plot(label='Moving Average DC1', linestyle='--', linewidth=1.2)
        if DC1_ewma:
            ewma1 = pd.ewma(dc1_resp, span=window_span, min_periods=periods)
            ewma1.plot(label='EW Moving Average DC1', linestyle='--', linewidth=1.2)
    if DC2:
        dc2_resp = df['DC2_Resp']
        if DC2_raw:
            dc2_resp.plot(label='Raw data DC2')
        if DC2_ma:
            roll2 = dc2_resp.rolling(center=False, window=window_span, min_periods=periods).mean()
            roll2.plot(label='Moving Average DC2', linestyle='--', linewidth=1.2)
        if DC2_ewma:
            ewma2 = pd.ewma(dc2_resp, span=window_span, min_periods=periods)
            ewma2.plot(label='EW Moving Average DC2', linestyle='--', linewidth=1.2)
    if DC3:
        dc3_resp = df['DC3_Resp']
        if DC3_raw:
            dc3_resp.plot(label='Raw data DC3')
        if DC3_ma:
            roll3 = dc3_resp.rolling(center=False, window=window_span, min_periods=periods).mean()
            roll3.plot(label='Moving Average DC3', linestyle='--', linewidth=1.2)
        if DC3_ewma:
            ewma3 = pd.ewma(dc3_resp, span=window_span, min_periods=periods)
            ewma3.plot(label='EW Moving Average DC3', linestyle='--', linewidth=1.2)
    os.system('clear')    
    plt.legend(loc='best')
    plt.xlabel('Time (in secs)')
    plt.ylabel('Average Response Times (in secs)')
    plt.tight_layout()
    if show: plt.show()
    if savefig: plt.savefig('./plots/'+ str(scheme) + '/plots_testbed_' + str(scheme) +'.png')
    plt.close()
    
plot(
    DC1=True, DC1_raw=False, DC1_ma=False, DC1_ewma=True, 
    DC2=True, DC2_raw=False, DC2_ma=False, DC2_ewma=True,  
    DC3=True, DC3_raw=False, DC3_ma=False, DC3_ewma=True,  
    window_span=100, periods=1, show=True, 
    # savefig=True, scheme='uniform_naive', label='Test-Bed: Response Times - Uniform-Site Migration (Naive)'
    # savefig=True, scheme='uniform_informed', label='Test-Bed: Response Times - Uniform-Site Migration (Informed)'
    # savefig=True, scheme='biased', label='Test-Bed: Response Times - Biased Migration'
    # savefig=True, scheme='single_point', label='Test-Bed: Response Times - Single-Point Migration'
    
)