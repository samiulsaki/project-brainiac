#!/bin/bash

# Copyright: Freeware (any user can download or distribute this script)
# Run the code at your own risk. Follow the instructions in README.
# Author: Samiul Saki
# Email: samiulsaki@gmail.com

# Script: Clean Instance-Group

function progress {
	echo "$cmd"
        eval "$cmd" > /dev/null 2>&1 & PID=$! #simulate a long process
        printf "PROGRESS: ["
        # While process is running...
        while kill -0 $PID 2> /dev/null ; do
	        printf  "▓"
        	sleep 1
        done
        printf "] done!\n\n"
}

# Gets all the initial setup values
read -e -p "Enter loadbalancer name [ENTER for recommended]: " -i "lb-dc" lb
read -e -p "Enter address name [ENTER for recommended]: " -i "lb-ip-cr" address_name
read -e -p "Enter backend-service name [ENTER for recommended]: " -i "web-map-backend-service" web_map
read -e -p "Enter url-map name [ENTER for recommended]: " -i "web-map" url_map
read -e -p "Enter target-http-proxy name [ENTER for recommended]: " -i "http-lb-proxy" target_proxy
read -e -p "Enter forwarding-rules name [ENTER for recommended]: " -i "http-cr-rule" forward_rule

resources=$(gcloud compute instance-groups list | grep resources | awk '{print $1}' | while read line ; do echo $line ; done) # Search for existing resources in the instance-groups list

printf "\nCleaning up Cross-Region:\n\n"

cmd="gcloud -q compute forwarding-rules delete $forward_rule" # Deletes the forwarding-rules with the given name
progress;

cmd="gcloud -q compute target-http-proxies delete $target_proxy" # Deletes the target-http proxies with the given name
progress;

cmd="gcloud -q compute url-maps delete $url_map" # Deletes the url-map with the given name
progress;

for i in $resources; do
	cmd="gcloud -q compute backend-services remove-backend $web_map --instance-group $i --instance-group-zone $(gcloud compute instance-groups list | grep ${i} | awk '{print $2}') --global" # Deletes all the backend-service instance-groups with the given url-map name
	progress;
done

cmd="gcloud -q compute backend-services delete $web_map --global" # Deletes the backend-service with the given url-map name
progress;

for i in $resources; do
	cmd="gcloud -q compute instance-groups unmanaged delete $i --zone $(gcloud -q compute instance-groups list | grep ${i} | awk '{print $2}')" # Deletes all the instance groups from the instance-groups
	progress;
done

cmd="gcloud -q compute health-checks delete http-basic-check" # Deletes the health-check
progress;

cmd="gcloud -q compute addresses delete $address_name --global" # Deletes the gloud address name with the given name
progress;

printf "\nDone! Cross-Region Cleaned.\n\n"
