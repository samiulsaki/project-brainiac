#!/bin/bash

# Copyright: Freeware (any user can download or distribute this script)
# Run the code at your own risk. Follow the instructions in README.
# Author: Samiul Saki
# Email: samiulsaki@gmail.com

# Script: Create Instance-Group

function progress {
	echo "$cmd"
        eval "$cmd" > /dev/null 2>&1 & PID=$! #simulate a long process
        printf "PROGRESS: ["
        # While process is running...
        while kill -0 $PID 2> /dev/null ; do
	        printf  "▓"
        	sleep 1
        done
        printf "] done!\n\n"
}

# Gets all the initial setup values
read -e -p "Enter loadbalancer name [ENTER for recommended]: " -i "lb-dc" lb
read -e -p "Enter address name [ENTER for recommended]: " -i "lb-ip-cr" address_name
read -e -p "Enter backend-service name [ENTER for recommended]: " -i "web-map-backend-service" web_map
read -e -p "Enter url-map name [ENTER for recommended]: " -i "web-map" url_map
read -e -p "Enter target-http-proxy name [ENTER for recommended]: " -i "http-lb-proxy" target_proxy
read -e -p "Enter forwarding-rules name [ENTER for recommended]: " -i "http-cr-rule" forward_rule

zones=$(gcloud compute instances list | tail -n+2 | grep $lb | grep -v TERMINATED | awk '{print $2}') # Finds all the data-centre region names

printf "\nSetting up Cross-Region:\n\n"

cmd="gcloud -q compute addresses create $address_name --ip-version=IPV4 --global" # Creates a address in gloud with the given name
progress;

eval "gcloud -q compute addresses list"

for i in $zones; do
	cmd="gcloud -q compute instance-groups unmanaged create $i-resources --zone $i" # Creates a instance-group for each region
	progress;
	lb_input=$(gcloud -q compute instances list | grep $i | grep $lb | awk '{print $1}')
	cmd="gcloud -q compute instance-groups unmanaged add-instances $i-resources --instances $lb_input --zone $i" # Adds the load-balancer instance of the same region n unmanaged instance-group
	progress;
done

eval "gcloud -q compute instance-groups list"

cmd="gcloud -q compute health-checks create http http-basic-check" # Creates a health check attribute
progress;

eval "gcloud -q compute health-checks list"

for i in $zones; do
	cmd="gcloud -q compute instance-groups unmanaged set-named-ports $i-resources --named-ports http:80 --zone $i" # For each instance group, define an HTTP service and map a port name to the relevant port
	progress;
done

cmd="gcloud -q compute backend-services create $web_map --protocol HTTP --health-checks http-basic-check --global" # Creates a backend service and specify its parameters. Set the --protocol field to HTTP because we are using HTTP to go to the instances
progress;

for i in $zones; do
	cmd="gcloud -q compute backend-services add-backend $web_map --balancing-mode UTILIZATION --max-utilization 0.5 --capacity-scaler 1 --instance-group $i-resources  --instance-group-zone $i --global" # Adds instance-groups as backends to the backend- services. A backend defines the capacity (max CPU utilization or max queries per second) of the instance-groups it contains.
	progress;
done

eval "gcloud -q compute backend-services list"

cmd="gcloud -q compute url-maps create $url_map --default-service $web_map" # Creates a default URL map that directs all incoming requests to all instances
progress;

cmd="gcloud -q compute target-http-proxies create $target_proxy --url-map $url_map"
progress;

eval "gcloud -q compute target-http-proxies list"

lb_ip_address=$(gcloud compute addresses list | grep $address_name | awk '{print $2}')
cmd="gcloud -q compute forwarding-rules create $forward_rule --address $lb_ip_address --global --target-http-proxy $target_proxy --ports 80" # Creates a target HTTP proxy to route requests to your URL map
progress;

eval "gcloud -q compute forwarding-rules list"

printf "\nDone! Cross-Region Set.\n\n"

echo "THIS MIGHT TAKE SOME TIME TO PROPAGATE ${address_name^^}. GIVE IT 10 MINUTES AND TRY AGAIN LATER."
