#!/bin/bash

# Copyright: Freeware (any user can download or distribute this script)
# Run the code at your own risk. Follow the instructions in README.
# Author: Samiul Saki
# Email: samiulsaki@gmail.com

# Script: CSV Generator

lb=$(gcloud compute instances list | grep lb-dc | grep -v TERMINATED | awk '{print $1}') # Finds all the load-balancers in the same network

for argh in $lb; do
        lb_ip=$(gcloud compute instances list | grep $argh | awk '{print $5}') # Find the IP address of a particular load-balancer
        rtime=$(eval "curl -sSL 'http://$lb_ip/haproxy?stats;csv;norefresh' | cut -d "," -f 2,61 | column -s, -t | grep worker | awk '{print \$2}' ") # Finds the number of back-end servers (workers) and the response time of each server of the load-balancer
        sum=0
        for i in $rtime; do
                sum=$((sum + i)) # Sum up all the workers total response time
        done
        echo "$(( sum / $(echo "${rtime}" | wc -l) ))" >> /home/ubuntu/$argh.csv # Calculates the average response time of the load-balancer and append to the CSV file
done
