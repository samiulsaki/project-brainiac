#!/bin/bash

# Copyright: Freeware (any user can download or distribute this script)
# Run the code at your own risk. Follow the instructions in README.
# Author: Samiul Saki
# Email: samiulsaki@gmail.com

# Script: DNS Setup

# Gets inital setup values
lb="lb-dc"
project="project-brainiac"
echo "Setting Cloud DNS Records for zone '${project}'"
ALL_OLD_RECORDS=$(gcloud dns record-sets list --zone=$project | tail -n+2 | awk '{print $4}' | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | while read line; do echo "\"${line}\""; done | tr '\n' ' ') # Finds all the A record-sets of the given project name
ALL_NEW_RECORDS=$(gcloud compute forwarding-rules list | grep http-cr-rule | awk '{print $2}') # Finds the cross-region (CR) load-balancing IP address

eval "gcloud dns record-sets list --zone=$project"
echo "Old Records:"
eval "echo $ALL_OLD_RECORDS"
eval "gcloud dns record-sets transaction start --zone=$project"
eval "gcloud dns record-sets transaction remove --zone=$project --name="project-brainiac.eu." --type=A --ttl=300 $ALL_OLD_RECORDS" # Removes the old A record-sets of the project
echo "New Records:"
eval "echo $ALL_NEW_RECORDS"
eval "gcloud dns record-sets transaction add --zone=$project --name="project-brainiac.eu." --type=A --ttl=300 $ALL_NEW_RECORDS" # Adds the CR IP address to new A record-sets of the project
eval "gcloud dns record-sets transaction execute --zone=$project"
eval "gcloud dns record-sets list --zone=$project" # Sets the A record-sets of the project
echo "Done! Records Set"

echo "THIS MIGHT TAKE SOME TIME TO PROPAGATE ${project^^}.EU. GIVE IT SOME MINUTES AND TRY AGAIN LATER."
