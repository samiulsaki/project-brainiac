#!/bin/bash

# Copyright: Freeware (any user can download or distribute this script)
# Run the code at your own risk. Follow the instructions in README.
# Author: Samiul Saki
# Email: samiulsaki@gmail.com

# Script: Testing

declare -a lb=('lb-dc1' 'lb-dc2' 'lb-dc3')

for argh in ${lb[@]}; do
        lb_ip=$(gcloud compute instances list | grep $argh | awk '{print $5}') # Finds all the load-balancer's IP addresses
        rtime=$(eval "curl -sSL 'http://$lb_ip/haproxy?stats;csv;norefresh' | cut -d "," -f 2,61 | column -s, -t | grep worker | awk '{print \$2}' ") # Curl the HAProxy stats in csv format running in the load-balancer
        sum=0
        for i in $rtime; do # Sum them together in a loop
                sum=$((sum + i))
        done
	eval "var=$(echo $argh | sed "s/lb-//g" | sed "s/.csv//g")" # Creates dc variables
	avg_resp=$(( sum / $(echo "${rtime}" | wc -l) )) # Calculates average response time of the laod-balancer
	div="1000"
	eval "${var}_resp=$(echo "($avg_resp) / $div" | bc -l)" # Convert the response time in seconds and insert in the dc_resp variable
	eval "${var}_worker=$(echo $(echo "${rtime}" | wc -l))" # Creates and add the number of workers in dc_worker variable
done

printf "`date '+%Y%m%d-%H%M%S'`,$dc1_resp,$dc1_worker,$dc2_resp,$dc2_worker,$dc3_resp,$dc3_worker\n" >> ./file.csv # Appeneds the data to the csv file
