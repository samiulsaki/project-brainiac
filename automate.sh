#!/bin/bash

# Copyright: Freeware (any user can download or distribute this script)
# Run the code at your own risk. Follow the instructions in README.
# Author: Samiul Saki
# Email: samiulsaki@gmail.com

# Script: Initial Setup

function progress {
	echo "$cmd"
	eval "$cmd" > /dev/null 2>&1 & PID=$! #simulate a long process
	printf "PROGRESS: ["
	# While process is running...
	while kill -0 $PID 2> /dev/null ; do
	    printf  "▓"
	    sleep 1
        done
        printf "] done!\n\n"
}

function instance_lists {
        echo "--------------------------------------------------------------------------------------------------------"
        eval "gcloud compute instances list"
        echo "--------------------------------------------------------------------------------------------------------"
}

clear

echo "--------------------------------------------------------------------------------------------------------"
echo "This is the initial infrustructure setup for the autonomous VMs. Follow the instructions."

instance_lists

# Gets all the initial setup values
read -e -p "Enter zone names [separated by comma]: e.g. " -i "us-central1-a,southamerica-east1-b,europe-west3-c,asia-northeast1-c" zone_name
z=$(echo "$zone_name" | sed 's/,/ /g' | wc -w )
read -e -p "Enter how many zones [Should match the previous agrument] " -i "$z" zone_number # Regions are separated by zone-names separated by commas
read -e -p "Enter images to use [ENTER for recommended]: " -i "ubuntu-console" image
read -e -p "Enter loadbalancer name [ENTER for recommended]: " -i "lb-dc" lb
read -e -p "Enter loadbalancer machine type [ENTER for recommended]: " -i "n1-standard-2" lb_machine
read -e -p "Enter consul-master name [ENTER for recommended]: " -i "consul-master-dc" consul
read -e -p "Enter consul-master machine type [ENTER for recommended]: " -i "n1-standard-1" consul_machine
read -e -p "Enter instance name [ENTER for recommended]: " -i "worker" inst
read -e -p "Enter instance machine type [ENTER for recommended]: " -i "f1-micro" inst_machine
read -e -p "Enter how many instances per loadbalancer [ENTER for recommended]: " -i "30" inst_number
read -e -p "Enter the variants name [ENTER uniform-naive/uniform-informed/biased/single-point]: " -i "single-point" variant # Migration script to imply in the network
read -e -p "Enter the service account email [ENTER for recommended]: " -i "my-instances@samiulsaki-148219.iam.gserviceaccount.com" serv_account

max_lb=$(gcloud compute instances list | grep $lb | awk '{print $1}' | grep -o '[0-9]*$' | jq -s max) # Gets total number of existing load-balancers to avoid data-centre name duplications
max_consul=$(gcloud compute instances list | grep $consul | awk '{print $1}' | grep -o '[0-9]*$' | jq -s max) # Gets total number of existing consul-masters to avoid consul server name duplications
k=1
l=1
echo "THIS MAY TAKE A WHILE, PLEASE BE PATIENT WHILE THE COMMANDS ARE RUNNING..."

./sidekicks/clean-instance-group.sh # Initiates instance-group cleanup process

for i in $(echo $zone_name | sed "s/,/ /g"); do
	printf "\nPopulating $i \n"
	echo "--------------------------------------------------------------------------------------------------------"
	cmd="gcloud -q compute instances create $consul$((max_consul + k)) --image $image --machine-type $consul_machine --zone $i --tags http-server --service-account $serv_account --scopes cloud-platform --metadata-from-file startup-script=./startups/consul-master.sh" # Creates a consul-master instance for each data-centre
	progress;
	
	cmd="gcloud -q compute instances create $lb$((max_lb + k)) --image $image --machine-type $lb_machine --zone $i --tags http-server --service-account $serv_account --scopes cloud-platform --metadata-from-file startup-script=./startups/lb.sh" # Creates a load-balancer instance for each data-centre
	progress;
	
	cmd="gcloud -q compute instances create nat-gateway-dc$((max_lb + k)) --network default --can-ip-forward --image $image --machine-type $consul_machine --zone $i --tags nat --service-account $serv_account --scopes cloud-platform --metadata-from-file startup-script=./startups/nat-gateway.sh" # Creates a nat-gateway instance for each zone
	progress;
	
	cmd="gcloud -q compute routes delete no-ip-internet-route-dc$((max_lb + k))" # Deletes the forwarding-route for the the specific zone if exists
	progress;
	
	cmd="gcloud -q compute routes create no-ip-internet-route-dc$((max_lb + k)) --network default --destination-range 0.0.0.0/0 --next-hop-instance nat-gateway-dc$((max_lb + k)) --next-hop-instance-zone $i --tags no-ip-dc$((max_lb + k)) --priority 800" # Creates a forwarding-route for all the worker instances that connects trhough nat-gateway instance
	progress;

	max_inst=$(gcloud compute instances list | grep $inst-dc$((max_lb + k)) | awk '{print $1}' | grep -o '[0-9]*$' | jq -s max) # Finds the number of existing worker instances in the data-centre
	l=1
	for j in $(eval echo {1..$inst_number}); do
		cmd="gcloud -q compute instances create $inst-dc$((max_lb +k))-$((max_inst + l)) --network default --no-address --image $image --machine-type $inst_machine --zone $i --tags no-ip-dc$((max_lb + k)) --service-account $serv_account --scopes cloud-platform --metadata-from-file startup-script=./startups/worker-$variant/worker-$variant.sh" # Creates worker instances with nat-gateway tags and service account properties
		progress;
		let "l++"
	done
	echo "--------------------------------------------------------------------------------------------------------"
	let "k++"
done

./sidekicks/create-instance-group.sh # Initiates instance-group setup process
./sidekicks/dns.sh # Initiates cloud DNS setup process
