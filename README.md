# Project-Brainiac
### A NSA masters thesis project

Run the `automate` script inside the folder:
```console
./automate.sh
```

It is considered that your project have a clean slate, i.e., no infrastructure in there. If it does the script will take care of it automatically.

If you don't want to include your previous deployment with the new setup then it is recommended that you remove the old setup or create a new project to deploy the new one.

If you like to add a new setup to the old setup, you can still run the automate script. The script will take care most of it for you. You do need to cleanup the instance groups and create new ones again along with the dns setup.

Your service account is yours to create and provide. So as other parameters like machine names, machine types, image types, etc.

Edit codes if needed.

Have fun!!!
