#!/bin/bash

# Copyright: Freeware (any user can download or distribute this script)
# Run the code at your own risk. Follow the instructions in README.
# Author: Samiul Saki
# Email: samiulsaki@gmail.com

function usage {
echo "
usage:
	gcl [ --create | --multi | --start | --stop | --delete | --all (stop/start/delete) | --show (list instances) | --route | -h or --help (for this message)]
	then follow the onscreen instructions

	:create/start/stop/delete for single instance
	:all for all the instances"
	return
}

function progress {
        eval "$cmd" > /dev/null 2>&1 & PID=$! #simulate a long process
        printf "PROGRESS: ["
        # While process is running...
        while kill -0 $PID 2> /dev/null ; do
	        printf  "▓"
        	sleep 1
        done
        printf "] done!\n\n"
}

function instance_lists {
	echo "--------------------------------------------------------------------------------------------------------"
	eval "gcloud compute instances list"
	echo "--------------------------------------------------------------------------------------------------------"
}

case "${1}" in

	--route )
		eval "gcloud compute routes list"
		read -e -p "Enter routes/s name [ENTER for recommended]: " -i "no-ip" route
		ALL_ROUTE=$(gcloud compute routes list | tail -n+2 | grep $route | awk '{print $1}')
		for argh in $ALL_ROUTE; do
			printf '\nrunning commnand: gcloud compute routes delete %s \n' "$argh"
			cmd="gcloud compute routes delete $argh"
			progress;
			shift
		done
		exit 1;;

	--create )
		instance_lists
		short=${1:2}
		echo "To ${short^^} single instance: "
		read -e -p "Enter instance name [ENTER for default followed by a number]: " -i "my-machine" name
		read -e -p "Enter image name [ENTER for default]: " -i "ubuntu-console" image
		read -e -p "Enter machine type [ENTER for default]: " -i "f1-micro" machine
		read -e -p "Enter zone [ENTER for default]: " -i "us-central1-a" zone
		echo "${1:2} single instance: in progress"
		echo "running command: gcloud compute instances ${1:2} $name --image $image --machine-type $machine --zone $zone --tags http-server"
		cmd="gcloud compute -q instances ${1:2} $name --image $image --machine-type $machine --zone $zone --tags http-server"
		progress;
		exit 1;;

	--multi )
		instance_lists
		echo -n "How many machines? [ENTER]: "
		read number

		echo "To CREATE multiple instances: "
		echo -n "Enter instance name. Just the name [ENTER]: "
		read name

		echo -n "Will set images. First Type regex or '.' for all) [ENTER]: "
		read image_regex
		images=$(gcloud compute images list | awk '{if (NR!=1) {print $1}}' | grep $image_regex)
		echo "${images[@]/%/$' \n'}" | column
		echo -n "Which image? Type full name [ENTER]: "
		read image

		echo -n "Will set machine-type. First Type regex (micro/small/standard/highcpu/highmem/etc/ or '.' for all) [ENTER]: "
		read machine_regex
		machines=$(gcloud compute machine-types list | awk '{if (NR!=1) {print $1}}' | grep $machine_regex | sort --unique)
		echo "${machines[@]/%/$' \n'}" | column
		echo -n "Which machine-type? Type full name [ENTER]: "
		read machine

		echo -n "Will set zones. First Type regex (europe/america/asia/central/etc/ or '.' for all) [ENTER]: "
		read zone_regex
		zones=$(gcloud compute zones list | awk '{if (NR!=1) {print $1}}' | grep $zone_regex | sort --unique)
		echo "${zones[@]/%/$' \n'}" | column
		echo -n "Which zone? Type full name [ENTER]: "
		read zone


		echo "CREATE multiple instances: in progress"
		max=$(gcloud compute instances list | grep $name | awk '{print $1}' | grep -o '[0-9]*$' | jq -s max)
		for i in $(eval echo {1..$number}); do
			echo "running command: gcloud compute instances create $name-$((max + i)) --image $image --machine-type $machine --zone $zone --tags http-server"
			cmd="gcloud -q compute instances create $name-$((max + i)) --image $image --machine-type $machine --zone $zone --tags http-server"
			progress;
		done
		exit 1;;

	--start )
		instance_lists
		short=${1:2}
		echo "To ${short^^} single instance: "
	        echo -n "Enter instance name [ENTER]: "
        	read name
	        echo -n "Which zone? [ENTER]: "
        	read zone
		echo "${1:2} single instance: in progress"
	        echo "running command: gcloud compute instances ${1:2} $name --zone $zone"
	        cmd="gcloud compute -q instances ${1:2} $name --zone $zone"
	        progress;
		exit 1;;

	--stop )
		instance_lists
		short=${1:2}
                echo "To ${short^^} single instance: "
		echo -n "Enter instances name [ENTER]: "
		read name
		echo -n "Which zone? [ENTER]: "
		read zone
		echo "${1:2} single instance: in progress"
		echo "running command: gcloud compute instances ${1:2} $name --zone $zone"
		cmd="gcloud -q compute instances ${1:2} $name --zone $zone"
		progress;
		exit 1;;

	--delete )
		instance_lists
		short=${1:2}
                echo "To ${short^^} single instance: "
	        echo -n "Enter instances name [ENTER]: "
        	read name
	        echo -n "Which zone? [ENTER]: "
	        read zone
		echo "${1:2} single instance: in progress"
	        echo "running command: gcloud compute instances ${1:2} $name --zone $zone"
	        cmd="gcloud -q compute instances ${1:2} $name --zone $zone"
	        progress;
		exit 1;;

	--all )
		instance_lists
		echo "Finding all instances..... Please Standby"
		read -e -p "Enter instance/s name [ENTER for recommended]: " -i "." inst_single
		ALL=$(gcloud compute instances list | awk '{if (NR!=1) {print $1, $2}}' | while read line ; do echo $line ; done | grep $inst_single | awk '{print $1}')
		#gcloud compute instances list | awk '{if (NR!=1) {print $1, $2}}' | while read line ; do echo $line ; done | grep lb | awk '{print $2}'
		#INST=$(for i in "$a"; do (gcloud compute instances list | grep "$i" | grep $inst_single | awk '{print $2}');  done)
		INST=$(gcloud compute instances list | awk '{if (NR!=1) {print $1, $2}}' | while read line ; do echo $line ; done | grep $inst_single | awk '{print $2}')
		printing=$(for i in '$ALL'; do (gcloud compute instances list | grep $inst_single | awk '{print $1 " of zone " $2}'); done)
		printf 'Instances selected are: \n%s\n' "$printing"
		echo -n "Sure? Type start/stop/delete for all instances [ENTER]: "
		read choice
		printf "Ain't nobody got time for that.....\nFeel free to press Ctrl+c anytime to abort waiting for operation.\nThe command will be executed and instances will $choice\n\n"
		set -- $INST
		for argh in $ALL; do
			printf '\nrunning commnand: gcloud compute instances %s %s --zone %s\n' "$choice" "$argh" "$1"
			cmd="gcloud -q compute instances $choice $argh --zone $1"
			progress;
			shift
		done
		exit 1;;

	--show )
		instance_lists;;

	-h | --help )
		usage;;

	* )
		usage;;

esac
